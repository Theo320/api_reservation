package com.example.reservation.controller;

import com.example.reservation.dao.SalleRepository;
import com.example.reservation.exception.ResourceNotFoundException;
import com.example.reservation.models.Reservation;
import com.example.reservation.models.Salle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SalleController {

    @Autowired
    private SalleRepository salleRepository;

    @GetMapping("/salles")
    public List<Salle> getSalles(){
        return salleRepository.findAll();
    }

    @GetMapping("/salles/{salleId}/reservations")
    public List<Reservation> getReservationsBySalle(@PathVariable(value = "salleId") Long id){
        return salleRepository.getById(id).getReservations();
    }

    @PostMapping(value = "/salle")
    public Salle saveSalle(@RequestBody Salle salle){
        System.out.println(salle);
        return salleRepository.save(salle);
    }

    @PutMapping(value = "/salles/{salleId}")
    public ResponseEntity<Salle> updateSalle(@PathVariable(value = "salleId") Long id, @RequestBody Salle salleDetails) throws ResourceNotFoundException{
        Salle salle = salleRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Salle not found " + id));

        salle.setName(salleDetails.getName());
        salle.setCapacity(salleDetails.getCapacity());

        final Salle updatedSalle = salleRepository.save(salle);
        return ResponseEntity.ok(updatedSalle);
    }

    @DeleteMapping(value = "/salles/{salleId}")
    public Map<String, Boolean> deleteSalle(@PathVariable(value = "salleId") Long id) throws ResourceNotFoundException{
       Salle salle = salleRepository.findById(id)
               .orElseThrow(() -> new ResourceNotFoundException("Salle not found ::" + id));

       salleRepository.delete(salle);
       Map<String, Boolean> response = new HashMap<>();
       response.put("deleted", Boolean.TRUE);
       return response;

    }

}