package com.example.reservation.controller;

import com.example.reservation.dao.ReservationRepository;
import com.example.reservation.dao.SalleRepository;
import com.example.reservation.exception.ResourceNotFoundException;
import com.example.reservation.models.Reservation;
import com.example.reservation.models.Salle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ReservationController {
    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private SalleRepository salleRepository;

    @GetMapping("/reservations")
    public List<Reservation> getReservations(){
        return reservationRepository.findAll();
    }

    @PostMapping(value = "/salles/{salleId}/reservation")
    public Reservation addReservation(@PathVariable(value = "salleId") Long salleId ,@RequestBody Reservation reservation){
        for(Reservation res: reservationRepository.findAll()){

            if(res.getDate().equals(reservation.getDate()) && res.getSalle().getId().equals(salleId)){
                throw new ResponseStatusException(HttpStatus.IM_USED);
            }
        }
        return salleRepository.findById(salleId).map(salle -> {
            reservation.setSalle(salle);
            return reservationRepository.save(reservation);
        }).orElseThrow(() -> new ResourceNotFoundException("salle id" + salleId));
    }

    @DeleteMapping(value = "/reservations/{reservationId}")
    public Map<String, Boolean> deleteReservation(@PathVariable(value = "reservationId") Long id) throws ResourceNotFoundException{
        Reservation reservation = reservationRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Reservation not found ::" + id));

        reservationRepository.delete(reservation);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;

    }



}
